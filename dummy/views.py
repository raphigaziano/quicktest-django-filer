from django.shortcuts import render

from dummy.models import Company

def foo(request):
    c = Company.objects.all()
    return render(request, 'home.html', {'companies': c})

